import React, { useState,useEffect } from "react";
import { Container, Nav, Navbar, Modal, Button, Form } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { useHistory } from "react-router-dom";
export default function NavigationBar() {
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [nama, setnama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState(Number);
  const [img, setImg] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);
  const handleClose2 = () => setShow1(false);
  const handleShow2 = () => setShow1(true);
  const [user, setUser] = useState([]);
  const history = useHistory();

const addList = async (e) => {
  e.preventDefault();
  e.persist();
  const fromData = new FormData()
  fromData.append("file", img)
  fromData.append("name", nama)
  fromData.append("harga", harga)
  fromData.append("deskripsi", deskripsi)
  try{
    await axios.post(
      "http://localhost:3008/product", fromData,
      { 
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          'Content-Type': "multipart/form-data"
        },
      }
    );
    setShow(false);
    Swal.fire("Good job!", "You clicked the button!", "success");
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  } catch (error) {
    console.log(error);
  }
};

const getAll = async () => {
  await axios 
  .get("http://localhost:3008/user/" + localStorage.getItem("userId"))
  .then((res) => {
    setUser(res.data.data)
 
  })
  .catch((error) => {
    // alert("Terjadi kesalahan " + error);
  });
};
console.log(user)
useEffect(() => {
  getAll();
}, [])
  const logout = () => {
    Swal.fire({
      icon: "success",
      title: " LogOut Berhasil!",
      showConfirmButton: false,
      timer: 1500,
         })
         setTimeout(() => {
          localStorage.clear();
          window.location.reload();
         }, 1500);
  };
  return (
    <div>
      <Navbar bg='dark' expand="lg">
        <img src="https://p4.wallpaperbetter.com/wallpaper/559/833/229/fate-series-minimalism-black-background-red-wallpaper-preview.jpg" alt=""  style={{ width: 80 }} />
        <Container>
          <Navbar.Brand href="/"  className=" text-white">Warung Sederhana</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/daftar" className=" text-white">Daftar Menu</Nav.Link>
             
              {localStorage.getItem("role") === "USER" && (
              <Nav.Link className="text-white" href="/cart"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
              <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
            </svg>
            </Nav.Link>
            )}
            </Nav>
            {localStorage.getItem("role") === "ADMIN" ? (
              <button  className="btn text-white" onClick={handleShow}>
                Tambah Menu
              </button>
            ) : (
              <></>
            )}
              {localStorage.getItem("role") === "USER" ? (
              <button  className="btn text-white" onClick={handleShow1}>
              Profil
              </button>
            ) :(
              <></>
            )}
           {localStorage.getItem("role") !== null ? (
             <a class="btn text-white" onClick={logout}>
             Logout
           </a>
           ) : (
            <a  className = "btn  text-white" href="/login">
            login
          </a>
           )} 
         
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Menu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form method="POST" onSubmit={addList}>
            <Form.Group className="mb-3">
              <Form.Label>Produk</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter nama"
                onChange={(e) => setnama(e.target.value)}
                value={nama}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Deskripsi</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter deskripsi"
                onChange={(e) => setDeskripsi(e.target.value)}
                value={deskripsi}
                required
              />
            </Form.Group>
            
            <Form.Group className="mb-3">
              <Form.Label>Harga</Form.Label>
              <Form.Control
                placeholder="Input Hrga"
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Image</Form.Label>
              <Form.Control
                type="file"
                placeholder="Input Image"
                onChange={(e) => setImg(e.target.files[0])}
                required
              />
            </Form.Group>
            {/* Untuk menghilangkan model */}
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            ||
            <Button variant="primary" type="submit">
              Save Changes
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
{/* Profil */}
      <Modal show={show1} onHide={handleClose1} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Profil</Modal.Title>
        </Modal.Header>
        <Modal.Body
       >
        <a
  href="#"
  class="relative block overflow-hidden rounded-lg border border-gray-900 p-8 bg-black"
>
  <span
    class="absolute inset-x-0 bottom-0 h-2 bg-gradient-to-r from-green-300 via-blue-500 to-purple-600"
  ></span>

  <div class="justify-between sm:flex ">
    <div>
      <h3 class="text-xl font-bold text-gray-100">
      {user.email}
      </h3>
    </div>

    <div class="ml-3 hidden flex-shrink-0 sm:block">
      <img
        alt="Paul Clapton"
        src={user.foto}
        class="h-16 w-16 rounded-lg object-cover shadow-sm"
      />
    </div>
  </div>

  <div class="mt-4 sm:pr-8">
    <p class="text-sm text-gray-100">
    Nama: {user.nama}
    <br />
    Alamat: {user.alamat}
    <br />
    No.Telepon: {user.nomor}
    </p>
  </div>
  <a href="/profil">
<Button className="text-center">
              Detail
            </Button>
            </a>
           
</a>
        </Modal.Body>
      </Modal>
    </div>
  );
}
