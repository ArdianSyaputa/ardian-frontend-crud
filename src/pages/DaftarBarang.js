import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2/dist/sweetalert2.js";

export default function Home() {
  const [list, setList] = useState([]);
  const [totalPage, setTotalpage] = useState(0);
  const history = useHistory();
  const [cari, setCari] = useState("");
  const [pagenow, setPagenow] = useState(0)

  const getAll = async (idx) => {
    await axios
      .get("http://localhost:3008/product/all?page=" + idx +"&search="+ cari)
      .then((res) => {
        setList(res.data.data.content);
        setTotalpage(res.data.data.totalPages);
        setPagenow(idx);
      })
      .catch((error) => {
        alert("Terjadi keasalahan" + error);
      });
  };
  const changePage = (e) => {
    getAll(e);
  };
  useEffect(() => {
    getAll(0);
  }, []);
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  const deleteUser = async (id) => {
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:3008/product/" + id);
          swalWithBootstrapButtons.fire(
            "Deleted!",
            "Your file has been deleted.",
            "success"
          );
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Your imaginary file is safe :)",
            "error"
          );
        }
      });

    getAll(0);
  };

  const loginn = () => {
    history.push("/login");
  };

function preview() {
 
  getAll(pagenow -1)
}
function nextpage() {

  getAll(pagenow +1)
}
  
  return (
    <div className="container my-5">
      <form class="mb-0 hidden lg:flex">
        <div class="relative">
          <input
            class="h-10 rounded-lg border-gray-200 pr-10 text-sm placeholder-gray-300 focus:z-10"
            placeholder="Search..."
            type="text"
            value={cari}
            onChange={(e) => setCari(e.target.value)}
          />

          <button
          onClick={(e) =>{
            e.preventDefault()
             getAll(0)}}
            type="submit"
            class="absolute inset-y-0 right-0 mr-px rounded-r-lg p-2 text-gray-600"
          >
            <span class="sr-only">Submit Search</span>
            <svg
              class="h-5 w-5"
              fill="currentColor"
              viewbox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                clip-rule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                fill-rule="evenodd"
              ></path>
            </svg>
          </button>
        </div>
      </form>

     
      <br />
      {list.length !== 0 ? (
        <>
         <h1 class="text-center text-2xl font-bold text-indigo-100 sm:text-3xl">
        Silahkan Memilih Menu Di Bawah
      </h1>
          <div className="grid grid-cols-4 gap-3">
            {list.map((makanan) => {
              return (
                <p class="relative block overflow-hidden group">
                  <img
                    src={makanan.img}
                    alt=""
                    class="object-cover w-full h-64 transition duration-500 group-hover:scale-105 sm:h-72"
                  />

                  <div class="relative p-6 bg-slate-900 border border-gray-100">
                    <h1 class="mt-4 text-lg font-medium text-gray-100">
                      {makanan.name}
                    </h1>

                    <h3 class="mt-1.5 text-sm text-white">
                      Rp.{makanan.harga}
                    </h3>

                    <p class="mt-1.5 text-sm text-gray-100">
                      {makanan.deskripsi}
                    </p>

                    {localStorage.getItem("role") === null ? (
                      <>
                        <button
                          onClick={loginn}
                          className="block w-full py-3 text-sm font-medium transition bg-yellow-400 rounded hover:scale-105"
                        >
                          Login Terlebih Dahulu
                        </button>
                      </>
                    ) : (
                      <>
                        {localStorage.getItem("role") === "ADMIN" ? (
                          <>
                            <button
                              onClick={() => deleteUser(makanan.id)}
                              className="block w-full py-3 text-sm font-medium transition bg-red-400 rounded hover:scale-105"
                            >
                              Hapus
                            </button>
                            <br />
                            <a
                              href={"/edit/" + makanan.id}
                              className="no-underline text-black"
                            >
                              <button className="block w-full py-3 text-sm font-medium transition bg-green-400 rounded hover:scale-105 ">
                                Ubah
                              </button>
                            </a>
                          </>
                        ) : (
                          <>
                            <a
                              href={"/tampilan/" + makanan.id}
                              className="no-underline text-black"
                            >
                              <button className="block w-full py-3 text-sm font-medium transition bg-yellow-400 rounded hover:scale-105">
                                Add to Cart
                              </button>
                            </a>
                          </>
                        )}
                      </>
                    )}
                  </div>
                </p>
              );
            })}
          </div>
          <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <span
            
            onClick={preview}
           
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100 "
          >
        
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </li>

        {createElements(totalPage, changePage)}
        <li>
          <span
     
           onClick={nextpage}
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
           
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </li>
      </ol>
        </>
      ) : (
        <>
          <h1 class="text-white">Menu Tidak Tersedia</h1>
        </>
      )}
    </div>
  );
}
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer text-white block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}
