import axios from "axios";
import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";

export default function Profil() {
  const [user, setUser] = useState([]);


  const getAll = async () => {
    await axios
      .get("http://localhost:3008/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data)

      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };


  console.log(user)
  useEffect(() => {
    getAll();
  }, [])
  return (
    <div
      href="/src/pages/Profile.js"
      class="relative block overflow-hidden rounded-lg border border-gray-100 p-6 mx-20"
    >
      <span class="absolute inset-x-0 bottom-0 h-2 bg-gradient-to-r from-purple-700 via-blue-600 to-purple-600"></span>

      <div class="justify-between text-center">
        {/* <div>
        <h3 class="text-xl font-bold text-white">
          Profil
        </h3>
  
        <p class="mt-1 text-xs font-medium text-white">By John Doe</p>
      </div>
   */}
        <div class="flex gap-5 mt-5">
          <img
            alt="Paul Clapton"
            src={user.foto}
            class="h-30 w-28 rounded-lg object-cover shadow-sm"
          />
          <div class=" mt-4 sm:pr-8">
            <h3 class="text-sm text-white flex">Email : {user.email}</h3>
            <h3 class="text-sm text-white flex">Nama : {user.nama}</h3>
            <h3 class="text-sm text-white flex">Alamat :  {user.alamat}</h3>
            <h3 class="text-sm text-white flex">Nomor : {user.nomor}</h3>
            {/* <h3 class="text-sm text-white flex">Deskripsi  : {user.deskripsi}</h3> */}
          </div>
        </div>
      </div>

      <dl class="mt-6 flex">


      </dl>
      <div class="text-left">
        <a href={"/editprofil/" + user.id}><Button>Edit</Button></a>
      </div>
      <div class="text-right">
        <a href="/daftar">
          <Button>Back</Button>
        </a>
      </div>

    </div>

  );
}
