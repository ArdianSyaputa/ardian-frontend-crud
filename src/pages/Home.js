import Carousel from 'react-bootstrap/Carousel';
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2/dist/sweetalert2.js";

export default function Home() {
  const [barang, setBarang] = useState([]);
  const [list, setList] = useState([]);
  const [totalPage, setTotalpage] = useState(0);
  const [cari, setCari] = useState("");
  const history = useHistory();

  const getAll = async (idx) => {
    await axios
      .get("http://localhost:3008/product/all?page=" + idx )
      .then((res) => {
        setList(res.data.data.content);
        setTotalpage(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi keasalahan" + error);
      });
  };
  const changePage = (e) => {
    getAll(e);
  };
  useEffect(() => {
    getAll(0);
  }, []);
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  const deleteUser = async (id) => {
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:3008/product/" + id);
          swalWithBootstrapButtons.fire(
            "Deleted!",
            "Your file has been deleted.",
            "success"
          );
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Your imaginary file is safe :)",
            "error"
          );
        }
      });

    getAll(0);
  };

  const loginn = () => {
    history.push("/login");
  };

  return (
      <div className="container my-5" >
        <h1 className='text-white'>Selamat Datang Di Warung Online</h1>
        <br />
        <Carousel fade>
      <Carousel.Item>
        <img
          className="d-block w-100 h-80"
          src="https://accurate.id/wp-content/uploads/2021/01/warung-makan-1.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100  h-80"
          src="https://cdn.idntimes.com/content-images/post/20171228/dscf5546-8c417efd89272b6d226313d6bb3509fe_600x400.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100  h-80"
          src="https://lelogama.go-jek.com/post_featured_image/General_1.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>

 <br />
      <br />
      {list.length !== 0 ? (
        <>
          <div className="grid grid-cols-4 gap-3">
            {list.map((makanan) => {
              return (
                <p class="relative block overflow-hidden group">
                  <img
                    src={makanan.img}
                    alt=""
                    class="object-cover w-full h-64 transition duration-500 group-hover:scale-105 sm:h-72"
                  />

                  <div class="relative p-6 bg-slate-900 border border-gray-100">
                    <h1 class="mt-4 text-lg font-medium text-gray-100">
                      {makanan.name}
                    </h1>

                    <h3 class="mt-1.5 text-sm text-white">
                      Rp.{makanan.harga}
                    </h3>

                    <p class="mt-1.5 text-sm text-gray-100">
                      {makanan.deskripsi}
                    </p>

                    {localStorage.getItem("role") === null ? (
                      <>
                        <button
                          onClick={loginn}
                          className="block w-full py-3 text-sm font-medium transition bg-yellow-400 rounded hover:scale-105"
                        >
                          Login Terlebih Dahulu
                        </button>
                      </>
                    ) : (
                      <>
                        {localStorage.getItem("role") === "ADMIN" ? (
                          <>
                            <button
                              onClick={() => deleteUser(makanan.id)}
                              className="block w-full py-3 text-sm font-medium transition bg-red-400 rounded hover:scale-105"
                            >
                              Hapus
                            </button>
                            <br />
                            <a
                              href={"/edit/" + makanan.id}
                              className="no-underline text-black"
                            >
                              <button className="block w-full py-3 text-sm font-medium transition bg-green-400 rounded hover:scale-105 ">
                                Ubah
                              </button>
                            </a>
                          </>
                        ) : (
                          <>
                            <a
                              href={"/tampilan/" + makanan.id}
                              className="no-underline text-black"
                            >
                              <button className="block w-full py-3 text-sm font-medium transition bg-yellow-400 rounded hover:scale-105">
                                Add to Cart
                              </button>
                            </a>
                          </>
                        )}
                      </>
                    )}
                  </div>
                </p>
              );
            })}
          </div>
        </>
      ) : (
        <>
          <h1>Belum Ada Data </h1>
        </>
      )}
      <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

        {createElements(totalPage, changePage)}
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
    </div>
  );
}
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer text-white block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}

