// import axios from "axios";
import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
// import Swal from "sweetalert2";

export default function Register() {
  const history = useHistory();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor, setNomor] = useState("");
  const [foto, setFoto] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const [img, setImg] = useState("");
  // const [userRegister, setUserRegister] = useState({
  //   email: "",
  //   password: "",
  //   role: "USER",
  //   nama:"",
  //   alamat:"",
  //   nomor:"",
  //   foto:""
  //   });

  // const handleOnChange = (e) => {
  //   setUserRegister((currUser) => {
  //     return { ...currUser,[e.target.id]: e.target.value };
  //   });
  // };

  const register = async (e) => {
    e.preventDefault();
    const fromData = new FormData()
    fromData.append("foto", foto)
    fromData.append("nama", nama)
    fromData.append("alamat", alamat)
    fromData.append("nomor", nomor)
    fromData.append("email", email)
    fromData.append("password", password)
    fromData.append("role", "USER")
    try {
    const response =  await axios.post("http://localhost:3008/user/sign-up",fromData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      if (response.status == 200) {
        alert("register succes");
        setTimeout(() => {
          history.push("/login");
        }, 1250);
      }
    } catch (err) {
      console.log(err);
    }
  };
  // const [registered, setRegistered] = useState({
  //   username: "",
  //   password: "",
  //   role: "user"
  // });

  // const history = useHistory();

  // const register = async (e) => {
  //   e.preventDefault();

  //   try {
  //     const { data, status } = await axios.post(
  //       `http://localhost:8000/akun`,
  //       registered,
  //       {
  //         headers: {
  //           "Content-Type": "application/json",
  //         },
  //       }
  //     );
  //     if (status === 200) {
  //       setRegistered((state) => {  Swal.fire(
  //           'Register Berhasil!',
  //           'Selamat Datang !' ,
  //           'success'
  //         )
  //         setTimeout(() => {
  //           window.location.reload();
  //         }, 1500);
  //         return [...state, data];
  //       });
  //     } else {
  //       throw Error("Something wrong");
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }
  //   history.push("/login")

  // };
  // const onChangeBoo = (e) => {
  //   setRegistered((currState) => {
  //     return { ...currState, [e.target.id]: e.target.value };
  //   });
  // };
  return (
    <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
      <div class="mx-auto max-w-lg">
        <h1 class="text-center text-2xl font-bold text-white sm:text-3xl">
          Register
        </h1>

        <p class="mx-auto mt-4 max-w-md text-center text-gray-500">
          Silahkan Masuk Dengan menggunakan akun, anda dapat Membeli!!
        </p>

        <form
          onSubmit={register}
          class="mt-6 mb-0 space-y-4 rounded-lg p-8 shadow-2xl"
        >
          <p class="text-lg font-medium text-white">Masuk ke akun anda!</p>

          <div>
            <Form.Label>
              <strong className="text-white">Email</strong>
            </Form.Label>

            <div class="relative mt-1">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  id="email"
                  placeholder="username"
                  onChange={(e) => setEmail(e.target.value)}
                  // cara json vs code
                  // value={registered.username}
                  // Cara Baru Menggunaakan intelej
                  value={email}
                />
              </InputGroup>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                  />
                </svg>
              </span>
            </div>
          </div>

          <div>
            <Form.Label>
              <strong className="text-white">Password</strong>
            </Form.Label>

            <div class="relative mt-1">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="password"
                  id="password"
                  placeholder="password"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                />
              </InputGroup>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div>
            <Form.Label>
              <strong className="text-white">Nama</strong>
            </Form.Label>

            <div class="relative mt-1">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  id="nama"
                  placeholder="Masukan Nama"
                  onChange={(e) => setNama(e.target.value)}
                  value={nama}
                />
              </InputGroup>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div>
            <Form.Label>
              <strong className="text-white">Alamat</strong>
            </Form.Label>

            <div class="relative mt-1">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  id="alamat"
                  placeholder="Masukan Alamat"
                  onChange={(e) => setAlamat(e.target.value)}
                  value={alamat}
                />
              </InputGroup>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div>
            <Form.Label>
              <strong className="text-white">Nomor Telepon</strong>
            </Form.Label>

            <div class="relative mt-1">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  id="alamat"
                  placeholder="Masukan Alamat"
                  onChange={(e) => setNomor(e.target.value)}
                  value={nomor}
                />
              </InputGroup>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div>
            <Form.Label>
              <strong className="text-white">Foto Profil</strong>
            </Form.Label>

            <div class="relative mt-1">
            <Form.Group className="mb-3">
              <Form.Control
                type="file"
                placeholder="Input Image"
                onChange={(e) => setFoto(e.target.files[0])}
                required
              />
            </Form.Group>

              <span class="absolute inset-y-0 right-4 inline-flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              </span>
            </div>
          </div>

          <button
            type="submit"
            class="block w-full rounded-lg bg-blue-700 px-5 py-3 text-sm font-medium text-white"
          >
            Sign in
          </button>

          <p class="text-center text-sm text-gray-500">
            Sudah Mempunyai Akun?
            <a class="underline" href="/login">
              Login
            </a>
          </p>
        </form>
      </div>
    </div>
  );
}
